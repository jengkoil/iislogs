//datepicker
$(function () {
    //Default Malay
    //$.fn.datepicker.defaults.language = 'ms';
    //English
    $.fn.datepicker.defaults.language = 'en';

    //Default Setting
    //var date = new Date();
    //var today = new Date(date.getFullYear(), date.getMonth(), date.getDate());
    var today = new Date();

    //Default orientation top
    var optComponent = {
        format: 'dd-M-yyyy',
        orientation: 'auto top',
        todayHighlight: true,
        autoclose: true,
    };

    //Orientation bottom
    var optComponentBottom = {
        format: 'dd-M-yyyy',
        orientation: 'auto bottom',
        todayHighlight: true,
        autoclose: true,
    };

    //Set Component
    //Orientation top
    $( '.datePicker-default, .datepicker-start, .datepicker-end, .datepicker-start1, .datepicker-end1, .datepicker-start2, .datepicker-end2, .datepicker-start-today, .datepicker-end-today', '.datepicker-past').datepicker( optComponent );
    $( '.datePicker-default, .datepicker-start, .datepicker-end, .datepicker-start1, .datepicker-end1, .datepicker-start2, .datepicker-end2, .datepicker-start-today, .datepicker-end-today', '.datepicker-past').attr("autocomplete", "off");

    
    //Single Date
    $(".datepicker-default").datepicker( optComponent );

    //Past Date
    $(".datepicker-past").datepicker(
        {
            format: 'dd-M-yyyy',
            orientation: 'auto top',
            endDate : today,
            todayHighlight: true,
            autoclose: true,
        }
    );

    //Start Date
    $(".datepicker-start").datepicker().on('changeDate', function (selected) {
        var minDate = new Date(selected.date.valueOf());
        $('.datepicker-end').datepicker('setStartDate', minDate);
    });

    //Start Date 1
    $(".datepicker-start1").datepicker().on('changeDate', function (selected) {
        var minDate1 = new Date(selected.date.valueOf());
        $('.datepicker-end1').datepicker('setStartDate', minDate1);
    });

    //Start Date 2
    $(".datepicker-start2").datepicker().on('changeDate', function (selected) {
        var minDate2 = new Date(selected.date.valueOf());
        $('.datepicker-end2').datepicker('setStartDate', minDate2);
    });

    //Start Date Today
    $(".datepicker-start-today").datepicker('setStartDate',today).on('changeDate', function (selected) {
        var minDateToday = new Date(selected.date.valueOf());
        $('.datepicker-end-today').datepicker('setStartDate', minDateToday);
    });

    //Start Date Today 1
    $(".datepicker-start-today1").datepicker('setStartDate',today).on('changeDate', function (selected) {
        var minDateToday1 = new Date(selected.date.valueOf());
        $('.datepicker-end-today1').datepicker('setStartDate', minDateToday1);
    });

    //End Date
    $(".datepicker-end").datepicker().on('changeDate', function (selected) {
        var minDate = new Date(selected.date.valueOf());
        $('.datepicker-start').datepicker('setEndDate', minDate);
    });

    //End Date 1
    $(".datepicker-end1").datepicker().on('changeDate', function (selected) {
        var minDate1 = new Date(selected.date.valueOf());
        $('.datepicker-start1').datepicker('setEndDate', minDate1);
    });

    //End Date 2
    $(".datepicker-end2").datepicker().on('changeDate', function (selected) {
        var minDate2 = new Date(selected.date.valueOf());
        $('.datepicker-start2').datepicker('setEndDate', minDate2);
    });

    //End Date Today
    $(".datepicker-end-today").datepicker().on('changeDate', function (selected) {
        var minDateToday = new Date(selected.date.valueOf());
        $('.datepicker-start-today').datepicker('setEndDate', minDateToday);
    });

    //End Date Today 1
    $(".datepicker-end-today1").datepicker().on('changeDate', function (selected) {
        var minDateToday1 = new Date(selected.date.valueOf());
        $('.datepicker-start-today1').datepicker('setEndDate', minDateToday1);
    });
    
    //////////////////////////////////////////////////// Orientation Bottom ///////////////////////////////////////////////////////////////////////
    //Orientation bottom
    $( '.datePicker-default-bottom, .datepicker-start-bottom, .datepicker-end-bottom, .datepicker-start1-bottom, .datepicker-end1-bottom, .datepicker-start2-bottom, .datepicker-end2-bottom, .datepicker-start-today-bottom, .datepicker-end-today-bottom', '.datepicker-past-bottom').datepicker( optComponentBottom );
    $( '.datePicker-default-bottom, .datepicker-start-bottom, .datepicker-end-bottom, .datepicker-start1-bottom, .datepicker-end1-bottom, .datepicker-start2-bottom, .datepicker-end2-bottom, .datepicker-start-today-bottom, .datepicker-end-today-bottom', '.datepicker-past-bottom').attr("autocomplete", "off");

    //Single Date
    $(".datepicker-default-bottom").datepicker( optComponentBottom );

    //Past Date
    $(".datepicker-past-bottom").datepicker(
        {
            format: 'dd-M-yyyy',
            orientation: 'auto top',
            endDate : today,
            todayHighlight: true,
            autoclose: true,
        }
    );

    //Start Date
    $(".datepicker-start-bottom").datepicker().on('changeDate', function (selected) {
        var minDate = new Date(selected.date.valueOf());
        $('.datepicker-end-bottom').datepicker('setStartDate', minDate);
    });

    //Start Date 1
    $(".datepicker-start1-bottom").datepicker().on('changeDate', function (selected) {
        var minDate1 = new Date(selected.date.valueOf());
        $('.datepicker-end1-bottom').datepicker('setStartDate', minDate1);
    });

    //Start Date 2
    $(".datepicker-start2-bottom").datepicker().on('changeDate', function (selected) {
        var minDate2 = new Date(selected.date.valueOf());
        $('.datepicker-end2-bottom').datepicker('setStartDate', minDate2);
    });

    //Start Date Today
    $(".datepicker-start-today-bottom").datepicker('setStartDate',today).on('changeDate', function (selected) {
        var minDateToday = new Date(selected.date.valueOf());
        $('.datepicker-end-today-bottom').datepicker('setStartDate', minDateToday);
    });

    //Start Date Today 1
    $(".datepicker-start-today1-bottom").datepicker('setStartDate',today).on('changeDate', function (selected) {
        var minDateToday1 = new Date(selected.date.valueOf());
        $('.datepicker-end-today1-bottom').datepicker('setStartDate', minDateToday1);
    });

    //End Date
    $(".datepicker-end-bottom").datepicker().on('changeDate', function (selected) {
        var minDate = new Date(selected.date.valueOf());
        $('.datepicker-start-bottom').datepicker('setEndDate', minDate);
    });

    //End Date 1
    $(".datepicker-end1-bottom").datepicker().on('changeDate', function (selected) {
        var minDate1 = new Date(selected.date.valueOf());
        $('.datepicker-start1-bottom').datepicker('setEndDate', minDate1);
    });

    //End Date 2
    $(".datepicker-end2-bottom").datepicker().on('changeDate', function (selected) {
        var minDate2 = new Date(selected.date.valueOf());
        $('.datepicker-start2-bottom').datepicker('setEndDate', minDate2);
    });

    //End Date Today
    $(".datepicker-end-today-bottom").datepicker().on('changeDate', function (selected) {
        var minDateToday = new Date(selected.date.valueOf());
        $('.datepicker-start-today-bottom').datepicker('setEndDate', minDateToday);
    });

    //End Date Today 1
    $(".datepicker-end-today1-bottom").datepicker().on('changeDate', function (selected) {
        var minDateToday1 = new Date(selected.date.valueOf());
        $('.datepicker-start-today1-bottom').datepicker('setEndDate', minDateToday1);
    });
});