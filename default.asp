<%
'----------------------------------------------------------------------------
' System Name: IIS Logs
' Date: 19/08/2021
' Developer: Jengkoil
'----------------------------------------------------------------------------
' Page Description:
' This page provides the functionality to read IIS Logs with code 500,404,200
'----------------------------------------------------------------------------
%>
<html>
<head>
<title>IIS Logs</title>
<link rel="stylesheet" type="text/css" href="assets/datatables/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="assets/datatables/css/dataTables.bootstrap4.min.css">

<script type="text/javascript" language="javascript" src="assets/datatables/js/jquery-3.5.1.js"></script>
<script type="text/javascript" language="javascript" src="assets/datatables/js/jquery.dataTables.js"></script>
<script type="text/javascript" language="javascript" src="assets/datatables/js/dataTables.bootstrap4.js"></script>

<script>
$(document).ready(function() {
    $('#logdata').DataTable( {
		"scrollY" : '65vh',
        "scrollX" : '100vh',
        "paging" : false,
		"ordering" : true,
		"order": [[ 0, "desc" ]]
    });
});

function funcCodeStatus(){
	var optCode = $('#optCode').val();
	var optDate = $('#optDate').val();

	if(optCode == "500"){
		window.location.href = "default.asp?code=500&date=" + optDate;
	}
	else if(optCode == "404"){
		window.location.href = "default.asp?code=404&date=" + optDate;
	}
	else if(optCode == "200"){
		window.location.href = "default.asp?code=200&date=" + optDate;
	}
	else{
		window.location.href = "default.asp";
	}
}
</script>

</head>
<body>
<%
paramDate = Request("date")
paramCode = Request("code")

pathName = "C:\inetpub\wwwroot\iislogs\W3SVC1\"

clientIP = ""
logDateTime = ""
refEnv = ""
refModule = ""
refFile = ""
errLine = ""
aspErrCode = ""
errDesc = ""
errCode = ""

'Get Current Date e.g: 210819
todayYYYYMMDD = Year(Now) & "-" & Month(Now) & "-" & Day(Now)
finalYear = Mid(Year(Now), 3)
finalMonth = Month(Now())
finalDay = Day(Now())

if (finalMonth < 10) then finalMonth = "0" & finalMonth 
if (finalDay < 10) then finalDay = "0" & finalDay 

todayDate = finalYear & finalMonth & finalDay 

if paramDate <> "" AND paramDate >= "2021-08-19" then
	splitParam = split(paramDate, "-")
	newParamDate = Mid(splitParam(0),3) & splitParam(1) & splitParam(2)

	if newParamDate > todayDate then
		fileDate = todayDate
	else
		fileDate = newParamDate
	end if
else
	Response.Redirect("default.asp?code=500&date=" & todayYYYYMMDD)
	'fileDate = todayDate
end if

'logFormat = "u_ex" 'for W3C
logFormat = "u_nc" 'for NCSA

fileName = logFormat & fileDate & ".log"
		
			'--------------------- Read Last Line in Log File ---------------------------
			Const ForReading = 1
			Set objFSO = Server.CreateObject ("Scripting.FileSystemObject")
			Set objLogFile = objFSO.OpenTextFile(pathName & fileName, ForReading)
			%>
			<div style="margin: 50 50 50 50">
			<div class="col-sm-1" style="position:absolute;">
			<select class="form-select form-select-sm mb-3" id="optCode" name="optCode" onchange="funcCodeStatus();">
				<option value="500" <% if Request("code") = "500" then Response.Write "selected" end if %>>500</option>
				<option value="404" <% if Request("code") = "404" then Response.Write "selected" end if %>>404</option>
				<!-- Disabled because large data -->
				<!-- <option value="200" <% if Request("code") = "200" then Response.Write "selected" end if %>>200</option>-->
			</select>
			<input type="hidden" id="optDate" name="optDate" value="<%= paramDate %>">
			</div>
			<div class="col-sm-8"></div>
			<table id="logdata" class="table table-striped" style="width:100%;">
			<thead>
				<tr>
					<th>Date / Time</th>
					<th>ErrLine</th>
					<th>ASP ErrCode</th>
					<th>Code Status</th>
					<th>Description</th>
					<th>Env</th>
					<th>Module</th>
					<th>Filename / Parameters</th>
					<th>Client IP</th>
				</tr>
			</thead>
			<tbody>

			<%
			Do Until objLogFile.AtEndOfStream
				strNextLine = objLogFile.ReadLine
				If Len(strNextLine) > 0 Then
					strLine = strNextLine

					splitLog = split(strNextLine)
					logDateTime = Mid(splitLog(3),2)
					clientIP = splitLog(0)
					logDetails = splitLog(6)
					errCode = splitLog(8)

					'For Err 500
					if paramCode = "" OR paramCode = "500" then
					if errCode = "500" then
						splitLogDetails = split(logDetails, "|")
						url = splitLogDetails(0)

						if Ubound(splitLogDetails) = 1 then
							errLine = splitLogDetails(1)
							aspErrCode =  splitLogDetails(2)
							errDesc = ""
						elseif Ubound(splitLogDetails) = 2 then
							errLine = splitLogDetails(1)
							aspErrCode = splitLogDetails(2)
							errDesc = ""
						elseif Ubound(splitLogDetails) = 3 then
							errLine = splitLogDetails(1)
							aspErrCode = splitLogDetails(2)
							errDesc = splitLogDetails(3)
						else
							errLine = ""
							aspErrCode = ""
							errDesc = ""
						end if

						splitURL = split(url, "/")
						splitURLength = Ubound(splitURL)
						refEnv = splitURL(1)
						refModule = splitURL(2)

						if splitURLength = 2 then
							isFile = InStr(1,splitURL(2),"?",1)
							if isFile <> 0 then
								refModule = ""
								refFile = splitURL(2)
							end if
						elseif splitURLength = 3 then
						refFile = splitURL(3)
						else
						refFile = ""
						end if
						%>

						<tr>
							<td><%= logDateTime %></td>
							<td><%= errLine %></td>
							<td><%= aspErrCode %></td>
							<td><%= errCode %></td>
							<td><%= errDesc %></td>
							<td><%= refEnv %></td>
							<td><%= refModule %></td>
							<td><%= refFile %></td>
							<td><%= clientIP %></td>
						</tr>
						<%
					end if
					end if

					''For Err 404 
					if paramCode = "404" OR paramCode = "999" then
					if errCode = "404" then
						splitURL = split(splitLog(6), "/")
						splitURLength = Ubound(splitURL)
						refEnv = splitURL(1)

						if splitURLength >= 2 then
							refModule = splitURL(2)
						end if

						refFile = splitLog(6)
						errLine = ""
						aspErrCode = ""
						errDesc = "File Not Found"

						'Hacker ir BOT
						if refEnv <> "logs" AND refEnv <> "vibes2dev" AND refEnv <> "vibes2" AND refEnv <> "vibes2training" then
							errDesc = "Hacker! OR BOT"
							errCode = "999"
						end if
				
						if refFile <> "/favicon.ico" then
						%>

						<tr>
							<td><%= logDateTime %></td>
							<td><%= errLine %></td>
							<td><%= aspErrCode %></td>
							<td><%= errCode %></td>
							<td><%= errDesc %></td>
							<td><%= refEnv %></td>
							<td><%= refModule %></td>
							<td><%= refFile %></td>
							<td><%= clientIP %></td>
						</tr>
						<%
						end if
					end if
					end if

					'For Err 200
					if errCode = "200" AND paramCode = "200" AND True then 'Disabled because large data
						splitURL = split(splitLog(6), "/")
						splitURLength = Ubound(splitURL)
						refEnv = splitURL(1)
						
						if splitURLength = 2 then
							isFile = InStr(1,splitURL(2),"?",1)
							if isFile <> 0 then
								refModule = splitURL(2)
								refFile = ""
							else
								refModule = ""
								refFile = splitURL(2)
							end if
						elseif splitURLength = 3 then
							refModule = splitURL(2)
							refFile = splitURL(3)
						end if

						errLine = ""
						aspErrCode = ""
						errDesc = "OK"
				
						if refFile <> "servertime.asp" AND refFile <> "" AND refFile <> "/favicon.ico" then
						%>

						<tr>
							<td><%= logDateTime %></td>
							<td><%= errLine %></td>
							<td><%= aspErrCode %></td>
							<td><%= errCode %></td>
							<td><%= errDesc %></td>
							<td><%= refEnv %></td>
							<td><%= refModule %></td>
							<td><%= refFile %></td>
							<td><%= clientIP %></td>
						</tr>
						<%
						end if
					end if

				End If
			Loop

			%>
			</tbody>
			</table>
			</div>
			<%
			objLogFile.Close
			%>
</body>
</html>